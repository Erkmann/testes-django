Django>=3.0,<4.0
django-cryptography==1.0
django-environ==0.4.5
djangorestframework==3.12.1
psycopg2==2.8.6
