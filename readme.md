Requer o Docker e o Docker-composer instalados

Comando para iniciar o Django pelo composer:

- sudo docker-compose up

Comando para iniciar o Django se baixado a imagem do Docker no gitlab:

- sudo docker run -d -p 8000:8000 dev python manage.py runserver 0.0.0.0:8000
