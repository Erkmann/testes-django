from django.contrib.auth.models import User, Group
from .models import Ble, Paciente
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class BleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Ble
        fields = '__all__'


class PacienteSerializer(serializers.Serializer):
    uid = serializers.IntegerField()
    imc = serializers.FloatField()
    diabetesB = serializers.BooleanField()

    def create(self, validated_data):
        return Paciente.objects.create(**validated_data)


class PacienteListSerializer(serializers.Serializer):
    name = serializers.CharField()
    uid = serializers.IntegerField()
    imc = serializers.FloatField()
    diabetesB = serializers.BooleanField()
