from django.contrib.auth.models import User, Group
from .models import Ble, Paciente
from rest_framework import viewsets, permissions, status
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import UserSerializer, GroupSerializer, \
    BleSerializer, PacienteSerializer, PacienteListSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class BleViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Ble.objects.all()
    serializer_class = BleSerializer


class PacienteList(APIView):

    def get(self, request):
        bles = Ble.objects.all()
        ps = []
        if bles[0]:
            for b in bles:
                paciente = Paciente.objects.filter(uid=b.id).first()
                name = b.name
                uid = 0
                imc = 0
                diabetesB = None

                if paciente:
                    uid = paciente.uid
                    imc = paciente.imc
                    diabetesB = paciente.diabetesB

                pp = {
                        'name': name,
                        'uid': uid,
                        'imc': imc,
                        'diabetesB': diabetesB
                }
                ps.append(pp)

        serializer = PacienteListSerializer(ps, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = PacienteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
