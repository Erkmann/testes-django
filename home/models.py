from django.db import models

from django_cryptography.fields import encrypt


class Ble(models.Model):
    name = models.CharField(max_length=150)
    name_crip = encrypt(models.CharField(max_length=150))
    idade = models.IntegerField(default=3)
    idade_crip = encrypt(models.IntegerField(default=3))
    nullable = models.CharField(max_length=50, blank=True, default='')
    nullable_crip = encrypt(models.CharField(
            max_length=50, blank=True, default='')
            )
    boool = models.BooleanField(default=False)
    boool_cript = encrypt(models.BooleanField(default=False))


class Paciente(models.Model):
    uid = models.IntegerField(default=None)
    imc = encrypt(models.FloatField(default=0))
    diabetesB = encrypt(models.BooleanField(blank=True))


Paciente.objects = Paciente.objects.using('pacientes')
